using import ..init

print "type"
    fixed u32 12
    
let num =
    fixedof u8 4 5.0
    
print "num" num

print "sum" (num + num)

print "comparison"
    (num - num) < (num + num)

let f = 
    fixed i32 8

print "7?"
    (f 1) + (f 2) * (f 3) # 7

print "0.3...?"
    (f 1) - (f 2) / (f 3) # about 1/3; 0.3...

print "4.0?"
    /
        f 2
        f 0.5

print "4?"
    //
        f 2
        f 0.5

print "0.25?"
    /
        f 0.5
        f 2

print "0?"
    //
        f 0.5
        f 2

print "as"
    (fixedof u8 4 0.25) as f

print "as"
    (fixedof i32 4 0.25) as f

print "as"
    (fixedof i32 16 0.25) as f
    
print "as"
    (fixedof i32 8 0.25) as f

print "as"
    (fixedof u8 8 0.25) as f

