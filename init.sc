using import utils.va
using import utils.functional
using import utils.utils

let fixed =
    typename "fixed"
        super = immutable

fn make-typename (base bits)
    typename
        .. (Any-string (Any base)) "." (Any-string (Any bits))
        super = fixed
        storage = base

fn make-fixed-type (base bits)
    let T =
        make-typename base bits
    let bitcount =
        bitcountof base
    set-type-symbol! T 'Base base
    set-type-symbol! T 'Integer (- bitcount bits)
    set-type-symbol! T 'Fraction bits

    T

fn make-fixed-value (T value)
    let val =
        (2 ** T.Fraction) * (value as f64) # TODO: will cause inaccuracy for big integer types?
    bitcast (val as T.Base) T

typefn fixed '__typecall (type ...)
    if (type == fixed)
        let base bits = ...
        if (base < integer)
            make-fixed-type base (i32 bits)
    else
        let val = ...
        if ((typeof val) == type)
            val
        else
            make-fixed-value type val

fn fixedof (base bits ...)
    (fixed base bits) ...

typefn fixed '__repr (value)
    repr (value as f32)

fn storage-fn (op result-type)
    fn (a b flipped?)
        let at bt =
            va-map typeof a b
        let ast bst =
            va-map storageof at bt
        let result = 
            op (bitcast a ast) (bitcast b bst) flipped?
        if ((typeof result) == result-type)
            result
        elseif ((typeof result) == ast)
            bitcast result at
        elseif ((typeof result) == bst)
            bitcast result bt

let storage-fn-bool =
    curry storage-fn
        result-type = bool


set-type-symbol! fixed '__+
    storage-fn '__+

set-type-symbol! fixed '__-
    fn (a b ...)
        if (none? b)
            bitcast
                - (storage a)
                typeof a
        else
            (storage-fn '__-) a b ...

set-type-symbol! fixed '__<
    storage-fn-bool '__<

set-type-symbol! fixed '__>
    storage-fn-bool '__>

set-type-symbol! fixed '__<=
    storage-fn-bool '__<=

set-type-symbol! fixed '__>=
    storage-fn-bool '__>=

set-type-symbol! fixed '__==
    storage-fn-bool '__==

set-type-symbol! fixed '__!=
    storage-fn-bool '__!=

set-type-symbol! fixed '__<<
    storage-fn '__<<

set-type-symbol! fixed '__>>
    storage-fn >>

set-type-symbol! fixed '__%
    storage-fn %

fn promote-type (int)
    match int

        u8 u16
        u16 u32
        u32 u64
        u8 u16
        u16 u32
        u32 u64

        i8 i16
        i16 i32
        i32 i64
        i8 i16
        i16 i32
        i32 i64

        else
            compiler-error! "Type not promotable"

fn fixed-mul (a b)
    let at bt =
        va-map typeof a b
    assert (at == bt) 

    let promoted =
        promote-type at.Base

    let extra-bits =
        (bitcountof promoted) - (bitcountof at)
        
    let ap bp =
        va-map (compose promoted storage) a b

    bitcast
        ((ap * bp) >> at.Fraction) as (storageof at)
        at
    

fn fixed-div (a b)
    let at bt =
        va-map typeof a b
    assert (at == bt) 

    let promoted =
        promote-type at.Base

    let ap bp =
        va-map (compose promoted storage) a b

    bitcast
        ((ap << at.Fraction) // bp) as (storageof at)
        at

set-type-symbol! fixed '__*
    gen-type-op2
        fixed-mul

set-type-symbol! fixed '__/
    fn (a b ...)
        if (none? b)
            / ((typeof a) 1) a
        else
            (gen-type-op2 fixed-div) a b ...

typefn fixed '__// (a b)
    (storage a) // (storage b)
    

typefn fixed '__as (val type)
    let T =
        typeof val
    if (type < integer)
        (val >> T.Fraction) as type
    elseif (type < real)
        ((storage val) as type) / ((2.0 as type) ** (T.Fraction as type))
    elseif (type < fixed)
        if (type.Fraction == T.Fraction)
            bitcast ((storage val) as type.Base) type
        elseif (type.Base == T.Base)
            let dir =
                type.Fraction - T.Fraction
            let storage =
                if (dir < 0)
                    (storage val) >> (- dir)
                else
                    (storage val) << dir
            bitcast storage type
        else
            (type (storage val)) >> T.Fraction

#dump
    vector-map (do *) (vectorof i32 1 2 3) (vectorof i32 3 2 1)


#fn casting (fun)
    fn (...)
        let T... =
            va-map typeof ...
        assert
            == T...
        let stypes 

typefn fixed '__vector+ (a b)
    let A B =
        va-map typeof a b
    assert
        == A B
    let As Bs =
        vector (storageof (element-type A 0)) (countof a)
        vector (storageof (element-type B 0)) (countof b)
    let va vb =
        bitcast a As
        bitcast b Bs
    
    bitcast
        va + vb
        A

typefn fixed '__vector- (a b)
    let A B =
        va-map typeof a b
    assert
        == A B
    let As Bs =
        vector (storageof (element-type A 0)) (countof a)
        vector (storageof (element-type B 0)) (countof b)
    let va vb =
        bitcast a As
        bitcast b Bs

    bitcast
        va - vb
        A

set-type-symbol! fixed '__vector* (curry vector-map (do *))
set-type-symbol! fixed '__vector/ (curry vector-map (do /))
set-type-symbol! fixed '__vector// (curry vector-map (do //))
set-type-symbol! fixed '__vector% (curry vector-map (do %))
set-type-symbol! fixed '__vector== icmp==
set-type-symbol! fixed '__vector!= icmp!=
set-type-symbol! fixed '__vector> (vector-signed-dispatch icmp>s icmp>u)
set-type-symbol! fixed '__vector>= (vector-signed-dispatch icmp>s icmp>=u)
set-type-symbol! fixed '__vector< (vector-signed-dispatch icmp<s icmp<u)
set-type-symbol! fixed '__vector<= (vector-signed-dispatch icmp<=s icmp<=u)



do
    let
        fixed
        fixedof

    locals;



